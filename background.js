function getLinkedinCookies(domain) {
  linkedin_cookie = {}

  chrome.cookies.getAll({}, function(allCookies) {
    for (var i = 0; i < allCookies.length; i++) {
      var obj = allCookies[i]
      if (obj.domain.indexOf(domain) !== -1) {
        linkedin_cookie[obj.name] = obj.value
      }
    }
  });

  return linkedin_cookie
}

//Will be used to get the linkedin HTML page
function UserHTML() {
  this.user_agents = {
    MAC_CHROME      : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36",
    MAC_FIREFOX     : "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6",
    WINDOWS_CHROME  : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.32 Safari/537.36",
    WINDOWS_FIREFOX : "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4b) Gecko/20030516 Mozilla Firebird/0.6",
    GOOGLEBOT       : "Googlebot/2.1 ( http://www.googlebot.com/bot.html)"
  }
}

UserHTML.prototype.fetch = function (url, cookies) {
  // console.log("URL : ", url)
  // console.log("Cookies : ", cookies)

  // var xmlHttp = new XMLHttpRequest();
  // xmlHttp.onreadystatechange = function() {
  //   if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
  //     // callback("http://localhost:3000/facebook_api/plugin", xmlHttp.responseText, logResponse);
  //     // callback(xmlHttp.responseText);
  //     console.log(xmlHttp.responseText);
  //   }
  //
  // xmlHttp.open("GET", url, true); // true for asynchronous
  // xmlHttp.setRequestHeader("Accept", "application/json");
  // xmlHttp.setRequestHeader("Content-Type","application/json");
  // xmlHttp.setRequestHeader("JSESSIONID", linkedin_cookie['JSESSIONID']);
  // xmlHttp.setRequestHeader("li_at", linkedin_cookie['li_at']);
  // xmlHttp.setRequestHeader("sl", linkedin_cookie['sl']);
  // xmlHttp.send(null);

  // window.location.href = url;
  // console.log(document.documentElement.outerHTML);

  // fetch(url).then(r => {
  //   return r.text();
  // }).then(html => {
  //   document.open("text/html");
  //   document.write(html);
  //   document.close();
  //
  //   const handler = function() {
  //     if (document.readyState !== "complete") {
  //       setTimeout(handler, 50);
  //     }
  //     else {
  //       const links = document.body.querySelectorAll("a");
  //       const urls = [].map.call(links, l => l.href);
  //       // Do something with the URLs.
  //       console.log(urls);
  //     }
  //   }
  //   setTimeout(handler, 50);
  // });

  var new_window = chrome.windows.create({url: url});
  var all_tab = chrome.windows.getAll(new_window);
  console.log(all_tab)

  // console.log(chrome.windows.getCurrent)
  // new_window.remove()
  // console.log(chrome.windows.get({url: url}));
  // console.log(document.documentElement.outerHTML);
};

var alarm_counter = 0;
var max_retrieve = 1;

// Eventhough delay is less than one minute,
// but in deployment (.crx), min delay is 1 min
if(alarm_counter == 0) {
    chrome.alarms.create("plugin_alarm", {delayInMinutes: 0, periodInMinutes: 0.5});
}

// Main function using alarm
chrome.alarms.onAlarm.addListener(function(alarm) {
  alarm_counter = alarm_counter + 1;
  console.log('Alarm ke-', alarm_counter)

  linkedin_cookie = getLinkedinCookies(".www.linkedin.com");
  console.log(linkedin_cookie)

  var user_html = new UserHTML()
  user_html.fetch("https://www.linkedin.com/in/geraldzakwan/", linkedin_cookie)

  chrome.alarms.clearAll();

  // Done after max_retrieve job executed
  if (alarm_counter == max_retrieve) {
    console.log('MASUK STOP ALARM')
    chrome.alarms.clear("plugin_alarm");
    chrome.alarms.clearAll();
    // alarm_counter = 0
  }
});

//Will be used to send the HTML page to the server
function postDataToScraper(theUrl, htmlPage, callback) {
    var xmlHttp = new XMLHttpRequest();
    // var params = "html_page=" + htmlPage;
    var params = "lorem=ipsum&name=binny";

    // console.log(params);

    xmlHttp.open("POST", theUrl, true);
    //Send the proper header information along with the request
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
            // console.log("DONE POSTING")
    }

    xmlHttp.send(params);
}
