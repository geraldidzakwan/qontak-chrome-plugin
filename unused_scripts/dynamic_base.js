function DynamicBase() {
  this.user_agents = {
    MAC_CHROME      : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36",
    MAC_FIREFOX     : "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6",
    WINDOWS_CHROME  : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.32 Safari/537.36",
    WINDOWS_FIREFOX : "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4b) Gecko/20030516 Mozilla Firebird/0.6",
    GOOGLEBOT       : "Googlebot/2.1 ( http://www.googlebot.com/bot.html)"
  }
}

DynamicBase.prototype.fetch = function (url, read, headers, follow_redirects, proxy) {
  var page = require('webpage').create()
  // url = "https://www.linkedin.com/in/geraldzakwan/"
  url = "http://geraldzakwan.github.io"

  page.open(url, function (status) {
      if (status !== 'success') {
          console.log('Unable to post!');
      } else {
          console.log(page.content);
      }
      phantom.exit();
  });
};

var db_test = new DynamicBase()
db_test.fetch()
