# README #

### Description ###

* Create plugin to silently scrap linkedin using user's cookies

### How to Use for Developer ###

* Clone this repository
* Open chrome settings
* Click the extension menu on the page's left side
* Enable developer mode on the top of the page
* Click 'Load unpacked extension' and select cloned folder
* Click 'Update extensions now'
* Enable the Qontak Chrome plugin
* In case the plugin doesn't run automatically, refresh it

### Notes ###

* You can either use one of these three kind of scripts :
* User page environment : This simply add your js to run in the html page user accessed
* Background environment : Run in isolate environment outside browser
* Extension environment : Like when you click the icon of the plugin

### Strategy ###

* Possibly will combine user page (content script) & background environment
